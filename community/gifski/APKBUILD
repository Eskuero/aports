# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=gifski
pkgver=1.11.0
pkgrel=0
pkgdesc="Highest-quality GIF encoder based on pngquant"
url="https://gif.ski/"
license="AGPL-3.0-or-later"
arch="all"
makedepends="cargo clang15-dev ffmpeg-dev"
source="https://github.com/ImageOptim/gifski/archive/$pkgver/gifski-$pkgver.tar.gz"

case "$CARCH" in
riscv64)
	options="$options textrels"
	;;
esac

_features="--features=video"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --frozen --release $_features
}

check() {
	cargo test --frozen $_features
}

package() {
	install -Dm755 target/release/$pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
9a150aace008034b1b50904fe8da7423118ddd905aa9cb661986135c98e331a5f61c293b4b75694dd7363c8a5605d06887d3781ee522318bba9b16e466fe42f0  gifski-1.11.0.tar.gz
"
