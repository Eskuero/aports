# Maintainer: psykose <alice@ayaya.dev>
pkgname=py3-orjson
pkgver=3.8.11
pkgrel=0
pkgdesc="Fast, correct Python JSON library supporting dataclasses, datetimes, and numpy"
url="https://github.com/ijl/orjson"
arch="all"
license="Apache-2.0 AND MIT"
makedepends="
	cargo
	py3-gpep517
	py3-maturin
	python3-dev
	"
checkdepends="
	py3-dateutil
	py3-numpy
	py3-pytest
	py3-tz
	tzdata
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/ijl/orjson/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/orjson-$pkgver"
options="net"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
b59cf2675c8d1e4cfd3b9cf3525b360a97c2191a8817e6df368ffed02095bc012c350df8ed57c992eec63b3233e67b528d50f8ebc45c78bc72b467c1d732f784  py3-orjson-3.8.11.tar.gz
"
