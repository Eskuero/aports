# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qmlkonsole
pkgver=23.04.0
pkgrel=0
pkgdesc="Terminal app for Plasma Mobile"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma-mobile/qmlkonsole"
license="GPL-3.0-or-later"
depends="
	qmltermwidget
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/qmlkonsole-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
92d9b89a997846b5eebbaab8e29015a958f831e6ec115000a5fcb981152a04aba6cbfa912668c8e25ff6322c5ffa5c9b5abecbb42d389094555e115178a7fcfa  qmlkonsole-23.04.0.tar.xz
"
