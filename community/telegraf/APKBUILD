# Contributor: Katie Holly <holly@fuslvz.ws>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>
pkgname=telegraf
pkgver=1.26.2
pkgrel=1
_commit=a3a884a18 # git rev-parse --short HEAD
_branch=release-${pkgver%.*}
pkgdesc="A plugin-driven server agent for collecting & reporting metrics, part of the InfluxDB project"
url="https://www.influxdata.com/time-series-platform/telegraf/"
arch="x86_64 aarch64"
license="MIT"
makedepends="go binutils-gold linux-headers"
checkdepends="tzdata"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
options="net"
source="telegraf-$pkgver.tar.gz::https://github.com/influxdata/telegraf/archive/v$pkgver.tar.gz
	tests-plugins-reverse-dns-ignore-result.patch
	telegraf.initd
	telegraf.confd
	"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local ldflags="-X main.version=$pkgver -X main.branch=$_branch -X main.commit=$_commit"
	go build -ldflags "$ldflags" ./cmd/telegraf

	# Generate sample config.
	./telegraf config >telegraf.conf
}

check() {
	go test -short ./...
}

package() {
	install -Dm755 "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
	install -Dm644 "$srcdir/$pkgname.confd" "$pkgdir/etc/conf.d/$pkgname"
	install -Dm755 "$builddir/$pkgname" "$pkgdir/usr/bin/$pkgname"
	install -Dm644 "$builddir/$pkgname.conf" "$pkgdir/etc/$pkgname.conf"
	install -dm755 "$pkgdir/etc/$pkgname.conf.d"
}

sha512sums="
bc4484b898d63dded9abd5ebc58c53e1e0fed68b8202da4c5d6a1d484e1355b55730e380d954c3bd75c1bf9446d4aa736aa82487a1bae9553b9b83974a610027  telegraf-1.26.2.tar.gz
a176dbaf580dc930a8b326d393083ed8e31bb0c1d8ee4d407aebb3c3af906e11652189e5557d4b99faaf44f27ed58b29192cac73ae328a41e582857cb915fa68  tests-plugins-reverse-dns-ignore-result.patch
abe483deb8e12fe140de2c36d17bbfbc97ed7a5de8c3d76162357f7ba6575b8236b7197b92a26ed6d54f95c1ccbfc12ca62d6cc0371bf49d10a1ea5622a51ed1  telegraf.initd
d1a9aa57f8b5179f2d8396518b9db757fe1c40337b515c1f750cf577683ff15f3174bc757afa70d880a1fef809c873e6aa0da1b903a5a97934c14965712d47a4  telegraf.confd
"
