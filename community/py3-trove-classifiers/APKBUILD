# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-trove-classifiers
pkgver=2023.4.29
pkgrel=0
pkgdesc="Canonical source for classifiers on PyPI"
url="https://github.com/pypa/trove-classifiers"
license="Apache-2.0"
arch="noarch"
depends="python3"
makedepends="py3-calver py3-gpep517 py3-installer py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/pypa/trove-classifiers/archive/$pkgver/py3-trove-classifiers-$pkgver.tar.gz"
builddir="$srcdir/trove-classifiers-$pkgver"

prepare() {
	default_prepare

	echo "Version: $pkgver" > PKG-INFO
}

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/trove_classifiers-$pkgver-py3-none-any.whl
}

sha512sums="
6456e4c0000402b196ad34f42136f0ed41e9f1e011b5c5a439b9d2da9dff44291cbedccd92dfa85720e29fe16889b5ca23cf7394d7d55713b09ad95b06300646  py3-trove-classifiers-2023.4.29.tar.gz
"
