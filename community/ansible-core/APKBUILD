# Contributor: Sean McAvoy <seanmcavoy@gmail.com>
# Maintainer: Sean McAvoy <seanmcavoy@gmail.com>
pkgname=ansible-core
pkgver=2.14.5
pkgrel=0
pkgdesc="core components of ansible: A configuration-management, deployment, task-execution, and multinode orchestration framework"
url="https://ansible.com"
options="!check" # for now
arch="noarch"
license="GPL-3.0-or-later"
depends="
	py3-cryptography
	py3-jinja2
	py3-packaging
	py3-paramiko
	py3-resolvelib
	py3-yaml
	python3
	"
makedepends="py3-setuptools"
subpackages="$pkgname-doc $pkgname-pyc"
source="https://pypi.python.org/packages/source/a/ansible-core/ansible-core-$pkgver.tar.gz
	resolvelib.patch
	"

replaces="ansible-base"

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/examples/
	cp -r examples/* \
	"$pkgdir"/usr/share/doc/$pkgname/examples/
	install -m644 README.rst "$pkgdir"/usr/share/doc/$pkgname

	mkdir -p "$pkgdir"/usr/share/man/
	local man
	for man in ./docs/man/man?/*.?; do
		install -Dm644 "$man" \
			"$pkgdir"/usr/share/man/man${man##*.}/${man##*/}
	done
}

sha512sums="
130a0de925c7229dee350ddbf4382d87802d86aab4817ed595105db8ed5071153847dac0478f20a994670e59e53a07b77d0bf35855d309ebc222fbb61233b7db  ansible-core-2.14.5.tar.gz
bfa9adabe9e94c663b822078019d0ac1677650e47869aa6419a7c1d9c3d494a6e58ce864f35e3c68b2c3f6587002bd3e9e4bfc763cad95008c395ef3be1827e0  resolvelib.patch
"
