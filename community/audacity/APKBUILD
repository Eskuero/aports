# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=audacity
pkgver=3.3.1
pkgrel=0
pkgdesc="Multitrack audio editor"
url="https://tenacityaudio.org/"
# s390x: fails to build
arch="all !s390x"
license="GPL-3.0-or-later"
makedepends="
	alsa-lib-dev
	cmake
	expat-dev
	ffmpeg-dev
	flac-dev
	jack-dev
	lame-dev
	libid3tag-dev
	libmad-dev
	libogg-dev
	libsndfile-dev
	libvorbis-dev
	lilv-dev
	lv2-dev
	mpg123-dev
	nasm
	portaudio-dev
	portmidi-dev
	samurai
	soundtouch-dev
	soxr-dev
	sqlite-dev
	suil-dev
	taglib-dev
	vamp-sdk-dev
	wavpack-dev
	wxwidgets-dev
	zlib-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
source="https://github.com/audacity/audacity/releases/download/Audacity-$pkgver/audacity-sources-$pkgver.tar.gz
	gcc13.patch
	"
# no tests
options="!check"
ldpath="/usr/lib/audacity"

provides="tenacity=$pkgver-r$pkgrel"
replaces="tenacity"

builddir="$srcdir"/audacity-sources-$pkgver

prepare() {
	default_prepare

	# hide aports version
	git init -q .
}

build() {
	case "$CARCH" in
	x86)
		local arch="-DHAVE_SSE=OFF -DHAVE_SSE2=OFF -DHAVE_MMX=OFF"
		;;
	x86_64)
		local arch="-DHAVE_SSE=ON -DHAVE_SSE2=ON -DHAVE_MMX=ON"
		;;
	esac

	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DAUDACITY_BUILD_LEVEL=2 \
		-Daudacity_conan_enabled=OFF \
		-Daudacity_has_vst3=OFF \
		-Daudacity_has_crashreports=OFF \
		-Daudacity_has_networking=OFF \
		-Daudacity_has_sentry_reporting=OFF \
		-Daudacity_has_updates_check=OFF \
		-Daudacity_lib_preference=system \
		-Daudacity_obey_system_dependencies=ON \
		-Daudacity_use_portsmf=local \
		-Daudacity_use_sbsms=local \
		-Daudacity_use_twolame=local \
		$CMAKE_CROSSOPTS \
		$arch

	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7def0553a37f3cfb3751da8332bcd8829f91d83f4247c9eb5ac1cd37ab67f7ac2e590516339a2f6c7e6f6ba9a850b14b9b4a80850c4222ef83596c18093562c7  audacity-sources-3.3.1.tar.gz
fc113c05e60ea07a56d224792f8f37dce49f6c4e072ccc101ff40b97b9cbf62aa85830bc2856995a04e9279e6e6af6d0474986c648adb7943e15088a3e61de05  gcc13.patch
"
