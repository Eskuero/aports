# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.41.0
pkgrel=1
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
arch="$arch !s390x"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"
options="net !check" # needs tinygo to turn go into wasm for tests

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -o trivy -ldflags "-X main.version=$pkgver" cmd/trivy/main.go
}

check() {
	go test ./...
}

package() {
	install -Dm755 trivy -t "$pkgdir"/usr/bin/
}

sha512sums="
76eaf89d7e969cf76bb6be8a26d9d5e26aedecb83775bd9adab825cd4e4508f6ba3668a1613342e94e362fe1e2daf3b51bbd5a94cbe89d1a50798bb6e2d0f6d4  trivy-0.41.0.tar.gz
"
