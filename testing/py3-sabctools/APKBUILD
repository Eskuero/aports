# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=py3-sabctools
_pkgname=sabctools
pkgver=7.0.2
pkgrel=0
pkgdesc="C implementations of functions for use within SABnzbd"
url="https://github.com/sabnzbd/sabctools"
arch="all"
license="GPL-2.0-or-later AND Apache-2.0 AND CC0-1.0"
makedepends="
	linux-headers
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="
	py3-chardet
	py3-jaraco.functools
	py3-portend
	py3-pytest
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/sabnzbd/sabctools/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
05968a29afb7a041549bae9e9a4303160820da4cd10ea33ab4475fe48fd0b4cf46023c6254715161265f3e1bdf2b66ae7e62007789fbc288aa229d3a218a3df8  py3-sabctools-7.0.2.tar.gz
"
